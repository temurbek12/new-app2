import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const state = () => ({
  color: '#E20505F7'
})

export const mutations = {
  SET_COLOR(state,id){
    state.color = id
  }
}
export const actions = {
  setColor({commit}, color){
    commit('SET_COLOR',color)
  }
}

export const getters = {
  getColor(state){
    return state.color
  }
}

